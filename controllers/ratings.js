const path = require("path")
import db from "../db/db.js"

class RatingsController {
    getAllRatings(request, response) {
        return response.status(200).send({
            success: "true",
            message: "retrieved all ratings successfully",
            ratings: db,
        });
    }
    getRating(request, response) {
        const rating = parseFloat(request.params.rating, 10);

        var collection = [];

        db.map((ratings) => {
            if (ratings.rating === rating) {
                collection.push({
                    name : ratings.name,
                    rating : ratings.rating,
                    comment : ratings.comment,
                });
            }
        });

        if (collection.length === 0) {
            return response.status(404).send({
                success: "false",
                message: `no games found with rating=${rating}`,
            });
        }

        return response.status(200).send({
            success: "true",
            message: "ratings successfully found",
            results: collection,
        });
    }
    createRating(request, response) {
        if (!request.body.rating) {
            return response.status(400).send({
                success: "false",
                message: "rating is required",
            });
        } else if (!request.body.name) {
            return response.status(400).send({
                success: "false",
                message: "name is required",
            });
        }

        db.map((ratings) => {
            if (ratings.name === request.body.name) {
                return response.status(400).send({
                    success: "false",
                    message: "duplicate",
                });
            }
        })

        const newRating = {
            id: db.length + 1,
            name: request.body.name,
            rating: parseInt(request.body.rating, 10),
            comment: request.body.comment ? request.body.comment : null,
        }   

        db.push(newRating);

        return response.status(204).send()
    }
}

const ratingsController = new RatingsController();
export default ratingsController;