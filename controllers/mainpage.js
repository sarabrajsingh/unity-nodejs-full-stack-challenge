const fs = require("fs")
const path = require('path');

class MainPageController {
    displayLogin(request, response) {
        return response.end(fs.readFileSync(path.dirname(__dirname) + "/views/index.html"));
    }
}

const mainPageController = new MainPageController();
export default mainPageController;