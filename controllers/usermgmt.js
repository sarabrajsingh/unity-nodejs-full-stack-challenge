import users from "../db/users.js"
const fs = require("fs")
const path = require("path")

class UserMgmt {
    checkLogin(request, response) {

        var isLoginable = false;

        users.map((user) => {
            // if userame/pwd is matchable, then allow login
            if (user.username === request.body.username) {
                if (user.password === request.body.password) {
                    if (user.login === false) {
                        isLoginable = true;
                    }
                }
            }
        });

        if(isLoginable) {
            return response.end(fs.readFileSync(path.dirname(__dirname) + "/views/ranking.html"));
        }

        return response.status(403).send();
    };
}

const userMgmt = new UserMgmt();
export default userMgmt;