import express from 'express';
import bodyParser from 'body-parser';
import router from "./routes/index.js"

const app = express();

// connect the express runtime to the middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/static', express.static('public'));
app.use(router);

const PORT = 5000;

app.listen(PORT, () => {
    console.log(`Application Server is Running on PORT=${PORT}`)
});