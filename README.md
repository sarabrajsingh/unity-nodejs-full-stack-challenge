# Foobar Game Rater

A very simple Node.js project that leverages an API server and a database written to the local filesystem; to allow users to log into website to submit a form that will rank games.

### About the Author

First attempt at putting together an application built in Node.js

### Prerequisites

A Node.js installation (v10.16.0 minimum) is required before running this web server. Please refer to the Node.js documentation specific to your platform:

https://nodejs.org/en/download/

Ensure that the **npm** package manager (v6.10.0 minimum) is installed as well.
## Getting Started

Grab the source code and execute the following commands to run the server:
```bash
npm install
npm run start
```

Using your favorite web browser, navigate to http://localhost:5000 to access the website.

**As of now, you can only log in with a single username/password - that is hardcoded in the database file (db/users.js)**

| USERNAME | PASSWORD |
| :---: | :---: |
| foobar | test123 |

## Endpoints

#### All Routes

- **[<code>GET</code> api/v1/ratings](https://gitlab.com/sarabrajsingh/unity-nodejs-full-stack-challenge/blob/master/controllers/ratings.js)**

    Example Request:
    ```bash
    curl -X GET -D- http://localhost:5000/api/v1/ratings
    ```
    Example Response:
    ```json
    {
        "success":"true",
        "message":"retrieved all ratings successfully",
        "ratings":[
            {
                "id":1,
                "name":"Rimworld",
                "rating":5,
                "comment":"dopest game developed on the Unity game engine"
            },
            {
                "..."
            }
        ]
    }
    ```
- **[<code>GET</code> api/v1/ratings/:rating](https://gitlab.com/sarabrajsingh/unity-nodejs-full-stack-challenge/blob/master/controllers/ratings.js)**

    Example Request:
    ```bash
    curl -X GET -D- http://localhost:5000/api/v1/ratings/4
    ```
    Example Response:
    ```json
    {
        "success":"false",
        "message":"no games found with rating=4"
    }
    ```
- **[<code>POST</code> api/v1/ratings](https://gitlab.com/sarabrajsingh/unity-nodejs-full-stack-challenge/blob/master/controllers/ratings.js)**
    
    Request Body:

    | Parameter | Type | Description |
    | :--- | :--- | :--- |
    | `name` | `string` | **Required**. Name of Game |
    | `rating` | `integer` | **Required**. Rating (0-5) |
    | `comment` | `string` | Your comments |
    
    Example Payload:
    ```json
    {
        "name" : "Cities Skylines",
        "rating" : 5,
        "comment" : "Move aside SimCity"
    }
    ```
    Example Request:
    ```bash
    curl -X POST -D- -H "Content-Type : application/json" -d@example.json http://localhost:5000/api/v1/ratings
    ```

    Example Response (Header):
    ```bash
    HTTP/1.1 204 No Content
    X-Powered-By: Express
    Date: Thu, 11 Jul 2019 19:35:46 GMT
    Connection: keep-alive
    ```


## Status Codes

| Status Code | Description |
| ----------- | ----------- |
| 200 | `OK`
| 204 | `NO CONTENT` |
| 400 | `BAD REQUEST` |
| 403 | `FORBIDDEN` |
| 404 | `NOT FOUND` |
| 500 | `INTERNAL SERVER ERROR` |

## Functional
* API Server
* Simple Login Page
* Feeback Submission

## Limitations
* Very rudementary login portal
* Non-persistent Database
* Ranking form submission once; User can just open another browser window and spam the submit button

## Improvments (TO-DO)
* At least, Basic Authencation
* Migrate DB to a proper datastore like MongoDB
* Find a better NPM module for FORMS submissions.
* Propagate Username and Password information to ranking.html for better session handling.
* Unit Tests!
* Containerization with Docker
* Refactor CSS and JavaScript code from HTML files into their own files
* Incorporate AngularJS as the frontend as it is better suited
## Built With

* [Node.js](https://nodejs.org) - The Web Framework Used
* [Javascript](https://www.javascript.com/) - Server-Side Coding Language

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Sarabraj Singh** - *Initial work* - [GitLab](https://gitlab.com/sarabrajsingh) [GitHub](https://github.com/sarabrajsingh)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details