import express from 'express';
import ratingsController from '../controllers/ratings.js';
import mainPageController from '../controllers/mainpage.js';
import userManagement from '../controllers/usermgmt.js'

const router = express.Router();

router.get("/", mainPageController.displayLogin);
router.get("/api/v1/ratings", ratingsController.getAllRatings);
router.get("/api/v1/ratings/:rating", ratingsController.getRating);
router.post("/api/v1/ratings", ratingsController.createRating);
router.post("/api/v1/login", userManagement.checkLogin);

export default router;